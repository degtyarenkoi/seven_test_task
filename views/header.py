#!/usr/bin/env python3
print('''
  <!DOCTYPE HTML>
    <html>
      <head>
        <meta charset="utf-8">
        <title>Company</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>
	    $('document').ready(function(){
		var nav, content;

		nav = $('nav#main');
		content = $('section#content');

		//Fetches and inserts content into container
		fetchAndInsert = function(href) {
		  $.ajax({
		    url: 'http://localhost:8000/cgi-bin/content/' + href.split('/').pop(),
		    method: 'GET',
		    cache: false,
		    success: function(data){
			if (href != "popup.py"){
		        	content.html(data);
		    	}
		    }
		  });
		};

		//Go back/forward
		$(window).on('popstate', function(){
		    fetchAndInsert(location.pathname);
		});

		nav.find('a').on('click', function(e) {
		    var href = $(this).attr('href');

		    //write into history
		    history.pushState(null, null, href);

		    //fetch and insert content
		    fetchAndInsert(href);

		    e.preventDefault();

		});
		//Pop up window
		$("#pup").click(function(e){
		    e.preventDefault();
		    pu = $('section#pu');
		    $.ajax({
		    	url: 'http://localhost:8000/cgi-bin/content/popup.py',
		   	 method: 'GET',
		   	 cache: false,
		   	 success: function(data){
		    	 	pu.html(data);
		    	 }
		    });

		    $('#popup').fadeIn("slow");
		    $('#hover').fadeIn("fast");
		});
		$("button").click(function(){
		    $('#popup').fadeOut("slow");
		    $('#hover').fadeOut("fast");
		});
	    });
	</script>


	<style>
	  #main ul li {
		display: inline-block;
		font-size: 16pt;
		margin: 0 50px 150px 0;		 
	  }
	  #main ul li:last-child {
		margin-left: 150px;
	  }
	  #content {
		display: block;
		width: 500px;
		margin: 50px;
	  }
	  #popup {
		display: none;
		width: 300px;
		height: 150px;
	 	border: 1px solid;
		text-align: center;
		background: #b3e0ff;		
		
		position: absolute;
		top: 50%;
		left: 50%;
		margin-left: -150px;
		margin-top: -75px;		
		z-index: 999;
	  }
	  #hover {
		display: none;
		height: 100%;
		width: 100%;
		position: absolute;
		top: 0;
		left: 0;
		background: rgba(0,0,0,.2);
		z-index: 998;
	  }
	</style>
      </head>
      <body>
	  <div id="hover"></div>
	  <nav id="main">
	    <ul>
	      <li><a href="home.py">Home</a></li>
	      <li><a href="aboutus.py">About us</a></li>
	      <li><a href="contacts.py">Contacts</a></li>
	      <li><a id="pup" href="popup.py">/popup\</a></li>
	    </ul>
	  </nav>
	    <div id="popup">
		<h3>Hi there!</h3>
		<button>close</button>
	    </div>
      </body>
    </html>
''')
